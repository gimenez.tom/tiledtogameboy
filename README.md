# TiledToGameBoy

Tiled extension to export for the GameBoy/GameBoyColor.

## Setup
- Put file "tiled_to_gameboy.js" in you extensions directory
  - For Windows: C:/Users/<USER>/AppData/Local/Tiled/extensions/
  - For Mac: ~/Library/Preferences/Tiled/extensions/
  - For Linux: ~/.config/tiled/extensions/

## Use

### Tilemap
- Click on "File > Export as..."
- In the field "Save as type" (just under field "File name", select "GameBoy tilemap format"

### Tileset
> Work in progress...